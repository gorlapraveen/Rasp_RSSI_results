#This script is used to Update the normal_velocity based on kalman_distance_update and time Difference, although we have kalma_velocity_update. Normal velocty is always comparable to the existing works 
import numpy as np
import math as mt
kalman_distance=open("kalman_distance_values.txt", "a+")
kalman_distance_list_lines=list(kalman_distance.readlines())
KD2=kalman_distance_list_lines[len(kalman_distance_list_lines)-1]
KD1=kalman_distance_list_lines[len(kalman_distance_list_lines)-2]
obj_timedif_distance_velocity=open("obj_timedif_distance_velocity.txt", "a+")
obj_timedif_distance_velocity_lines=list(obj_timedif_distance_velocity.readlines())
dt=obj_timedif_distance_velocity_lines[len(obj_timedif_distance_velocity_lines)-3]
normal_velocity_update_value=((float(KD2))-(float(KD1)))/(float(dt))
normal_velocity_update_value=abs(normal_velocity_update_value)
normal_velocity_update=open("normal_velocity_update.txt", "a+")
normal_velocity_update.write(str(normal_velocity_update_value)+"\n")
normal_velocity_update.close()
print "Normal Velocity Update = ", normal_velocity_update_value

