##Processing th eRSSI values from the file to claculate the satndard deviation and for precesion adjust ments

import numpy as np
import math as mt
import time as tm
import subprocess as sp
RS_Calb_array=[]
RS_recv_latest=[]
RS_recv_lines_append=[]
####Inital calibration using known samples of RSSI at distance 1m 
#RS_Calb=open("/home/debian/Documents/test.txt","r")
RS_Calb=open("raspap_calibration.txt","r")
for lines in RS_Calb.readlines():
   for i in lines.split():
      RS_Calb_array.append(float(i))
StDev_Calb=np.std(RS_Calb_array)
#print RS_Calb_array
#print StDev_Calb
RS_Calb_min_With_StD=np.mean(RS_Calb_array)-StDev_Calb
RS_Calb_max_With_StD=np.mean(RS_Calb_array)+StDev_Calb
#print "The Min RSSI at distance 1m is = ", RS_Calb_min_With_StD
#print "The Max RSSI at Distance 1m is = ", RS_Calb_max_With_StD
######Now Fetching  Instantaneous RSSI values and calculating   Standard deviations and the mean
#RS_recv=open("/home/debian/Documents/test.txt","r")
RS_recv=open("raspap_realtime.txt","r")
for RS_recv_lines in RS_recv.readlines():
 for i in RS_recv_lines.split():
    RS_recv_lines_append.append(float(i))
     #RS_recv_lines_append.append(i)
for i in range (1,4):
 RS_recv_latest.append(RS_recv_lines_append[len(RS_recv_lines_append)-i])
#print RS_recv_latest
StDev_recv=np.std(RS_recv_latest)
#print StDev_recv
RS_recv_min_With_StD=np.mean(RS_recv_latest)-StDev_recv
RS_recv_max_With_StD=np.mean(RS_recv_latest)+StDev_recv

print "The Min RSSI at distance D meters is =", RS_recv_min_With_StD
print "The Max RSSI at Distance D meters is =", RS_recv_max_With_StD

#####################################################################################
n=4;
Time= tm.time()
original_time=open("original_time.txt", "a+")
original_time.write(str(Time)+"\n")
original_time.close()
Distance_recv=mt.pow(10,((RS_Calb_min_With_StD)-(RS_recv_min_With_StD))/(10*n))
time_distance=open("time_distance.txt","a+")
time_distance.write("\n"+str(Distance_recv)+"\n")
time_distance.write(str(Time))
time_distance.close()
print "Object is in the radius of ", Distance_recv, "Meters"
time_distance=open("time_distance.txt","a+")
time_distance_list_lines=list(time_distance.readlines())
T2=time_distance_list_lines[len(time_distance_list_lines)-1]
D2=time_distance_list_lines[len(time_distance_list_lines)-2]
T1=time_distance_list_lines[len(time_distance_list_lines)-3]
D1=time_distance_list_lines[len(time_distance_list_lines)-4]
time_distance.close()
print T2, T1, D2, D1
obj_timedif_distance_velocity=open("obj_timedif_distance_velocity.txt", "a+")
dt=float(T2)-float(T1)
obj_timedif_distance_velocity.write(str(dt)+'\n')
obj_timedif_distance_velocity.write(str(D2))
Vel=((float(D2))-(float(D1)))/((float(T2))-(float(T1)))
Vel =abs(Vel)
obj_timedif_distance_velocity.write(str(abs(Vel))+'\n')
print "Instantaneous Velocity = ", Vel
# The Below is just to print the  previous Distance and velocity to the sepearte purpose for offlien 
normal_distance=open("normal_distance.txt","a+")
normal_distance.write(str(Distance_recv)+'\n')
normal_distance.close()
normal_velocity=open("normal_velocity.txt","a+")
normal_velocity.write(str(Vel)+"\n")
normal_velocity.close()
