import matplotlib.pyplot as plt
import numpy as np

kalman_distance=open("kalman_distance_values_08_march_2019_1.txt", "a+") #Update based on Kalman Distance
kalman_distance_readlines=kalman_distance.readlines()
kalman_distance_readlines_value=[line.split()[0] for line in kalman_distance_readlines]
kalman_distance_readlines_value=np.array(kalman_distance_readlines_value)
kalman_distance_readlines_value=kalman_distance_readlines_value.astype(np.float)

normal_distance=open("normal_distance_08_march_2019_1.txt", "a+")
normal_distance_readlines=normal_distance.readlines()
normal_distance_readlines_value=[line.split()[0] for line in normal_distance_readlines]
normal_distance_readlines_value=np.array(normal_distance_readlines_value)
normal_distance_readlines_value=normal_distance_readlines_value.astype(np.float)

time_interval=open("time_interval_08_march_2019_1.txt", "a+")
time_interval_readlines=time_interval.readlines()
time_interval_readlines_value=[line.split()[0] for line in time_interval_readlines]
time_interval_readlines_value=np.array(time_interval_readlines_value)
time_interval_readlines_value=time_interval_readlines_value.astype(np.float)


N1=len(kalman_distance_readlines_value)
N2=len(normal_distance_readlines_value)
N3=len(time_interval_readlines_value)
print N1
print N2
print N3
ind =np.arange(N1)
p1=plt.plot(ind, kalman_distance_readlines_value, linestyle='-',label='Kalman Filtered', marker='o')
p2=plt.plot(ind, normal_distance_readlines_value,linestyle='--',label='Real Time ', marker='D')
plt.ylabel('Distance (m)')
plt.xlabel('Time (s)')
plt.yticks(np.arange(0, 19, 3))
plt.xticks(ind, time_interval_readlines_value)
plt.legend()
plt.grid(linewidth=0.3)
plt.show()
