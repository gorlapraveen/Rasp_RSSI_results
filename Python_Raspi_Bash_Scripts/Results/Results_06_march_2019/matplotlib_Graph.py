import matplotlib.pyplot as plt
import numpy as np

kalman_distance=open("kalman_distance_values_6_mar_2019_1.txt", "a+")
kalman_distance_readlines=kalman_distance.readlines()
kalman_distance_readlines_value=[line.split()[0] for line in kalman_distance_readlines]
kalman_distance_readlines_value=np.array(kalman_distance_readlines_value)
kalman_distance_readlines_value=kalman_distance_readlines_value.astype(np.float)

normal_distance=open("normal_distance_6_mar_2019_1.txt", "a+")
normal_distance_readlines=normal_distance.readlines()
normal_distance_readlines_value=[line.split()[0] for line in normal_distance_readlines]
normal_distance_readlines_value=np.array(normal_distance_readlines_value)
normal_distance_readlines_value=normal_distance_readlines_value.astype(np.float)

N1=len(kalman_distance_readlines_value)
N2=len(normal_distance_readlines_value)
print N1
print N2
ind =np.arange(N1)
p1=plt.plot(ind, kalman_distance_readlines_value, 2)
plt.ylabel('Kalman Filtered Real time distance')
plt.xlabel('Real time distance')
plt.title('Kalman Filteration')
plt.xticks(normal_distance_readlines_value)
plt.legend((p1), ('Kalman Filtered output'))
plt.show()
