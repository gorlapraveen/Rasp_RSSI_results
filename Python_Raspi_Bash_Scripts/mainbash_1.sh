#!/bin/bash
case "$1" in
 --calibrate)
  echo 'In calibration mode'
  echo 'executing raspap_calibration.sh'
  #./raspap_calibration.sh !#
  Counter=0
  while [[ $Counter -le 1 ]]; do
      ./raspap_calibration.sh
  done
  ;;
 --realtime)
   echo 'In realtime mode'
   Counter=0
   while [[ $Counter -le 1 ]]; do
      sed -i '/NONE/Id' kalman_distance_values.txt #sed to remove unwanted dumb, observed is `NONE` word
      sed -i '/NONE/Id' kalman_velocity_values.txt #Same comment as last sed command!
      ./raspap_realtime.sh
      python mainscript_1.py
      python kalman_filter_design.py
      python normal_velocity_update.py
      ./time_interval.sh
      
      
      echo '======================='
   done
   ;;
   #./raspap_realtime.sh; python mainscript_1.py; python kalman_filter_design.py; !
  *)
  echo 'It seems that you used a wrong way!. USe the following formates'
  echo
  echo './mainbash_1.sh --calibrate #for Calibrate mode'
  echo 
  echo './mainbash_1.sh --realtime #for Realtime mode including Kalman Filteration'
  ;;
esac
#./raspap.sh
#python mainscript_1.py
#python kalman_filter_design.py
#./mainbash_1.sh



