#!/bin/bash
time_initial=$(sed -n 2p original_time.txt) #Find first line , that is initial start time in real world time
number_of_time_points=$(wc -l < original_time.txt) #count the number of lines 
time_now=$(sed -n "$number_of_time_points"p original_time.txt) #finding the latest time in the real world sense
time_interval=$(awk '{printf("%10.2f\n",$1-$2)}' <<<"$time_now $time_initial") #after above step, here it finds the time interval difference between inital time and current time.
echo $time_interval >>time_interval.txt #printing the time lien to a file
