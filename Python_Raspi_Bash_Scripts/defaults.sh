#!/bin/bash
case "$1" in
  --all)
   echo "Setting Every *.log and *.txt files to default ..."
   echo "Within 5 secs, Press CTRL+D to stop Here"
   sleep 5
   echo ". . ."
   
   echo " "

   echo " Setting Kalman_distance_values.txt to default..."
   bash -c "date >> Log_Files/kalman_distance_values.txt"
   bash -c "cat kalman_distance_values.txt >> Log_Files/kalman_distance_values.txt"
   echo "Appended kalman_distance_values.txt values to Log_Files folder"
   echo > kalman_distance_values.txt
   echo 1 >>kalman_distance_values.txt
   echo "Kalman_distance_values is now in default with one initial Kalman Position as  value as 1m"
   echo ". . ."
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo " "
   
   echo " Setting kalman_velocity_values.txt to default..."
   bash -c "date >>Log_Files/kalman_velocity_values.txt "
   bash -c "cat kalman_velocity_values.txt >> Log_Files/kalman_velocity_values.txt"
   echo "Appended kalman_velocity_values.txt values to  Log_Files folder"
   echo > kalman_velocity_values.txt
   echo 0 >>kalman_velocity_values.txt
   echo "kalman_velocity_values.txt is now in default with one initial Kalman Velocity value as 0 m/sec"
   echo ". . ."
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo " "

   echo " Setting time_distance.txt to default..."
   bash -c "date >> Log_Files/time_distance.txt"
   bash -c "cat time_distance.txt >> Log_Files/time_distance.txt"
   echo "Appended time_distance.txt values to  Log_Files folder"
   echo > time_distance.txt
   echo "time_distance.txt is now in default with null"
   echo ". . ."
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo " "

   echo " Setting obj_timedif_distance_velocity.txt to default..."
   bash -c "date >> Log_Files/obj_timedif_distance_velocity.txt"
   bash -c "cat obj_timedif_distance_velocity.txt >> Log_Files/obj_timedif_distance_velocity.txt"
   echo "Appended obj_timedif_distance_velocity.txt values to  Log_Files folder"
   echo > obj_timedif_distance_velocity.txt
   echo "obj_timedif_distance_velocity.txts is now in default with null value"
   echo ". . ."
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo " "
   
   echo " Setting original_time.txt to default..."
   bash -c "date >> Log_Files/normal_velocity.txt"
   bash -c "cat original_time.txt >> Log_Files/original_time.txt"
   echo "Appended normal_velocity.txt values to  Log_Files folder"
   echo > original_time.txt
   echo "original_time.txt is now in default with null"
   echo ". . ."
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo " "
   
   echo " Setting time_interval.txt to default..."
   bash -c "date >> Log_Files/time_interval.txt"
   bash -c "cat normal_velocity.txt >> Log_Files/time_interval.txt"
   echo "Appended time_interval.txt values to  Log_Files folder"
   echo > time_interval.txt
   echo "time_interval.txt is now in default with null"
   echo ". . ."
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo " "
   

   echo " Setting normal_distance.txt to default..."
   bash -c "date >> Log_Files/normal_distance.txt"
   bash -c "cat normal_distance.txt >> Log_Files/normal_distance.txt"
   echo "Appended normal_distance.txt values to  Log_Files folder"
   echo > normal_distance.txt
   echo "normal_distance.txt is now in default with null"
   echo ". . ."
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo " "

   echo " Setting normal_velocity.txt to default..."
   bash -c "date >> Log_Files/normal_velocity.txt"
   bash -c "cat normal_velocity.txt >> Log_Files/normal_velocity.txt"
   echo "Appended normal_velocity.txt values to  Log_Files folder"
   echo > normal_velocity.txt
   echo "normal_velocity.txt is now in default with null"
   echo ". . ."
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo " "
   
   echo " Setting normal_velocity_update.txt to default..."
   bash -c "date >> Log_Files/normal_velocity_update.txt"
   bash -c "cat normal_velocity_update.txt >> Log_Files/normal_velocity_update.txt"
   echo "Appended normal_velocity_update.txt values to  Log_Files folder"
   echo > normal_velocity_update.txt
   echo "normal_velocity_update.txt is now in default with null"
   echo ". . ."
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo " "
   
   echo " Setting raspap_calibration.txt to default..."
   bash -c "date >> Log_Files/raspap_calibration.txt"
   bash -c "cat raspap_calibration.txt >> Log_Files/raspap_calibration.txt"
   echo "Appended raspap_calibration.txt values to  Log_Files folder"
   echo > raspap_calibration.txt
   echo "raspap_calibration.txt is now in default with null"
   echo ". . ."
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo " "
   
   echo " Setting raspap_realtime.txt to default..."
   bash -c "date >> Log_Files/raspap_realtime.txt"
   bash -c "cat raspap_realtime.txt >> Log_Files/raspap_realtime.txt"
   echo "Appended raspap_realtime.txt values to  Log_Files folder"
   echo > raspap_realtime.txt
   echo "raspap_realtime.txt is now in default with null"
   echo ". . ."
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo " "

   echo ". . . . . . . . . . . . . . . . . . ."
   echo "Restored these to defaults : kalman_distance_values.txt, kalman_velocity.txt, time_distance.txt, obj_timedif_distance_velocity.txt, normal_distance.txt, normal_velocity.txt, raspap_calibration.txt and raspap_realtime.txt   "
   ;;
 --help)
   echo "run 'default.sh' for making kalman_distance_values.txt, kalman_velocity.txt, time_distance.txt, obj_timedif_distance_velocity.txt, normal_distance.txt, normal_velocity.txt, to defalt initial state leaving  'raspap_calibration.txt' &  'raspap_realtime.txt' in the current state  "
   echo "run 'default.sh --all' for making every *.log and *.txt files to its default initail state..."
   echo "run 'default.sh --help' for any help"
   ;;
   *) 
   echo "Setting *.log and *.txt files to default state except 'raspap_calibration.txt' & 'raspap_realtime.txt' ..."
   echo "Within 5 secs, Press CTRL+D to stop Here"
   sleep 5
   echo ". . ."
   
   echo " "

   echo " Setting Kalman_distance_values.txt to default..."
   bash -c "date >> Log_Files/kalman_distance_values.txt"
   bash -c "cat kalman_distance_values.txt >> Log_Files/kalman_distance_values.txt"
   echo "Appended kalman_distance_values.txt values to Log_Files folder"
   echo > kalman_distance_values.txt
   echo 1 >>kalman_distance_values.txt
   echo "Kalman_distance_values is now in default with one initial Kalman Position as  value as 1m"
   echo ". . ."
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo " "
   
   echo " Setting kalman_velocity_values.txt to default..."
   bash -c "date >>Log_Files/kalman_velocity_values.txt "
   bash -c "cat kalman_velocity_values.txt >> Log_Files/kalman_velocity_values.txt"
   echo "Appended kalman_velocity_values.txt values to  Log_Files folder"
   echo > kalman_velocity_values.txt
   echo 0 >>kalman_velocity_values.txt
   echo echo "kalman_velocity_values.txt is now in default with one initial Kalman Velocity value as 0 m/sec"
   echo ". . ."
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo " "

   echo " Setting time_distance.txt to default..."
   bash -c "date >> Log_Files/time_distance.txt"
   bash -c "cat time_distance.txt >> Log_Files/time_distance.txt"
   echo "Appended time_distance.txt values to  Log_Files folder"
   echo > time_distance.txt
   echo "time_distance.txt is now in default with null"
   echo ". . ."
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo " "

   echo " Setting obj_timedif_distance_velocity.txt to default..."
   bash -c "date >> Log_Files/obj_timedif_distance_velocity.txt"
   bash -c "cat obj_timedif_distance_velocity.txt >> Log_Files/obj_timedif_distance_velocity.txt"
   echo "Appended obj_timedif_distance_velocity.txt values to  Log_Files folder"
   echo > obj_timedif_distance_velocity.txt
   echo "obj_timedif_distance_velocity.txts is now in default with null value"
   echo ". . ."
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo " "
   
    echo " Setting original_time.txt to default..."
   bash -c "date >> Log_Files/normal_velocity.txt"
   bash -c "cat original_time.txt >> Log_Files/original_time.txt"
   echo "Appended normal_velocity.txt values to  Log_Files folder"
   echo > original_time.txt
   echo "original_time.txt is now in default with null"
   echo ". . ."
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo " "
   
   echo " Setting time_interval.txt to default..."
   bash -c "date >> Log_Files/time_interval.txt"
   bash -c "cat normal_velocity.txt >> Log_Files/time_interval.txt"
   echo "Appended time_interval.txt values to  Log_Files folder"
   echo > time_interval.txt
   echo "time_interval.txt is now in default with null"
   echo ". . ."
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo " "   

   echo " Setting normal_distance.txt to default..."
   bash -c "date >> Log_Files/normal_distance.txt"
   bash -c "cat normal_distance.txt >> Log_Files/normal_distance.txt"
   echo "Appended normal_distance.txt values to  Log_Files folder"
   echo > normal_distance.txt
   echo "normal_distance.txt is now in default with null"
   echo ". . ."
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo " "

   echo " Setting normal_velocity.txt to default..."
   bash -c "date >> Log_Files/normal_velocity.txt"
   bash -c "cat normal_velocity.txt >> Log_Files/normal_velocity.txt"
   echo "Appended normal_velocity.txt values to  Log_Files folder"
   echo > normal_velocity.txt
   echo "normal_velocity.txt is now in default with null"
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo ". . ."


   echo " Setting normal_velocity_update.txt to default..."
   bash -c "date >> Log_Files/normal_velocity_update.txt"
   bash -c "cat normal_velocity_update.txt >> Log_Files/normal_velocity_update.txt"
   echo "Appended normal_velocity_update.txt values to  Log_Files folder"
   echo > normal_velocity_update.txt
   echo "normal_velocity_update.txt is now in default with null"
   echo ". . ."
   echo "Within 3 secs, Press CTRL+D to stop Here"
   sleep 3
   echo "Proceeding next"
   echo ". . ."
   echo " "


   echo ". . ."
   echo ". . ."
   echo " "
   echo ". . . . . . . . . . . . . . . . . . . "
   echo "--------------------------------------"
   echo "Restored to Default : 'kalman_distance_values.txt', 'kalman_velocity_values.txt', 'time_distance.txt', 'obj_distance_timediff.txt', 'normal_distance.txt', 'normal_velocity.txt'"
   echo "Not Restored to default: 'raspap_calibration.txt' & 'raspap_realtime.txt' are kept same"
   echo "--------------------------------------"
   ;;
esac
