import numpy as np
import sys
class kalman_filter_design:
 def par_measure(self):
   n_types=2 #velocity and distance
   n_objects=1 ##For simplicity, can fetch from the file
   obj_timedif_distance_velocity=open("obj_timedif_distance_velocity.txt", "a+")
   obj_timedif_distance_velocity_lines=list(obj_timedif_distance_velocity.readlines())

   ##Xpprevious=np.random.rand(1,n_objects) ##replace with the original values for a file
   kalman_distance_values=open("kalman_distance_values.txt", "a+")
   kalman_distance_values_lines=list(kalman_distance_values.readlines())
   Xpprevious=kalman_distance_values_lines[len(kalman_distance_values_lines)-1]
   Xprealtime=obj_timedif_distance_velocity_lines[len(obj_timedif_distance_velocity_lines)-2] ##Replaced with original value

   ##Xvprevious=np.random.rand(1,n_objects)##replace with original values from file
   kalman_velocity_values=open("kalman_velocity_values.txt", "a+")
   kalman_velocity_values_lines=list(kalman_velocity_values.readlines())
   Xvprevious= kalman_velocity_values_lines[len(kalman_velocity_values_lines)-1]
   Xvrealtime=obj_timedif_distance_velocity_lines[len(obj_timedif_distance_velocity_lines)-1] ##Replaced with original value
   print ("X Previous Kalman ==")
   print Xpprevious
   print ("V Previous Kalman ==")
   print Xvprevious
   print("X previous realtime==")
   print Xprealtime
   print("Velocity previous realtime==")
   print Xvrealtime
   print("")
   A=np.array([[1]])
   Q=np.array([[0.01]]) #Amount of noise in the system
   R=np.array([[0.1]])   #Noise in sensors as covariance matrix
   H=np.array([[1,0]])
   #dt=0.51 ##replace with the original values from the file
   dt=obj_timedif_distance_velocity_lines[len(obj_timedif_distance_velocity_lines)-3]
   ##Xcrealtime=np.random.rand(n_types,n_objects) ##replace with the original values for a file
   Xcrealtime=np.vstack((float(Xprealtime), float(Xvrealtime)))
   #print "Xcrealtime =" Xcreatime1] D2=time_distance_list_lines[len(time_distance_list_lines)-2] T1=time_distance_list_lines[len(time_distance_list_lines)-3] D1=time_distance_list_lines[len(time_distance_list_lines)-4]
 #def kf_state_prediction(self, n_objects, Xpprevious, Xvprevious, A, H, B, Q, dt):
   Xcprevious=np.vstack((float(Xpprevious),float(Xvprevious))) ##Vertical adding the matrix i.e position, velocity
   print Xcprevious
   Xcpredict=np.dot(np.array([[float(A),float(dt)],[float(0),float(1)]]),np.array(Xcprevious)) #Predcting the positioning
   Pprevious=np.array([[1,0],[0,1.96]]) ##If unaware of inital distance, let it be =1m,Average speed of human is ro=1.4m/s,(ro)^2=1.96 
   Ppredict=np.multiply(A,np.multiply(Pprevious,np.transpose(A))) +Q
 #def kf_observation(self, Xcrealtime, H, R):
   Yinv=Xcrealtime-np.dot(H,Xcpredict) ## H is used for transf into measmt matrix, so H=[1,0] because Yinv is dist, V comp=0.
   Yinv_co=np.dot(H,np.dot(Ppredict,np.transpose(H))) + R
   kf_gain=np.dot(Ppredict,np.dot(np.transpose(H),np.linalg.inv(Yinv_co)))
 #def kf_update(self, Xpredict, kf_gain, Yinv):
   Xupdate=Xcpredict+np.multiply(kf_gain,Yinv)
   Pupdate=np.dot((np.identity(Ppredict.shape[0])-np.dot(kf_gain,H)),Ppredict)
 #def Xupdate_split(self, Xupdate, n_types):
   Xpupdate, Xvupdate=np.split(Xupdate, n_types)
   print("Xpupdate=")
   #print Xpupdate
  # print np.savetxt(sys.stdout, Xpupdate, fmt="%.7f")
   a = np.savetxt(sys.stdout, Xpupdate, fmt="%.7f")
   print a
   example=open("sed_test_file.txt", "a+")
   example.write(str(a)+'\n')
   kalman_distance_values.write(str(np.savetxt(sys.stdout, Xpupdate, fmt="%.7f")))
   print("XVupdate===")
   print np.savetxt(sys.stdout, Xvupdate, fmt="%.7f")
   kalman_velocity_values.write(str(np.savetxt(sys.stdout, Xvupdate, fmt="%.7f")))
   return
kalman_filter_design=kalman_filter_design()
kalman_filter_design.par_measure()
sys.exit

