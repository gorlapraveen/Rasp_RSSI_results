## Note

**There are two Branches**
      
1. `Master`

2. `Rasp_RSSI_results`

3. `testing`

4. `testing-2` . This is initiated to reduce `Raspberry pi` based erors. Here the erros are minimized.

**Most Updated Branch is** `testing-2` . Use `testing-2` .

** Released ** tags or Versions, `v2` `Version 2`

**In `testing-2` the mostly used folder is `Python_RAspi_Bash_Scripts`** , In which it contaisn automatic scripts for RSSI calibration and Realtime such as `mainbash_1.sh` . This main `mainbash_1.sh` contains `calibration_mode` and `Real_time_mode` with `kalaman filteration` using the existing scripts such as `raspap_calibration.sh`, `raspap_realtime`,`mainscript_1.py`(For RSSI mean, distance and Time Calculation) and `kalman_filter_design.py`.
  
    There are some files with `.txt` and `.log` for loging the calibrated infor, realtime info and Temporary calculation info.


--------------------------


**In `testing` the mostly used folder is `Python_RAspi_Bash_Scripts`** , In which it contaisn automatic scripts for RSSI calibration and Realtime such as `mainbash_1.sh` . This main `mainbash_1.sh` contains `calibration_mode` and `Real_time_mode` with `kalaman filteration` using the existing scripts such as `raspap_calibration.sh`, `raspap_realtime`,`mainscript_1.py`(For RSSI mean, distance and Time Calculation) and `kalman_filter_design.py`.
  
    There are some files with `.txt` and `.log` for loging the calibrated infor, realtime info and Temporary calculation info.

------------------------------------
# Configuration Steps:

* Run  `defaults.sh` to make `.log` and `.txt` files to default state, except `raspap_calibrate.txt` and `raspap_realtime.txt`.
* Run  `defaults.sh --all` to make `.log` and `.txt` files to default state including `raspap_calibrate.txt` and `raspap_realtime.txt`.
* Run  `mainbash_1.sh --calibrate` : for calibrating the RSSI values(`raspaap_calibrate.sh`) at preferably at 1m, results output to a file `raspap_calibrate.txt`
* Run `mainbash_1.sh --realtime` : Following will be automatically done.
    1. for realtime (`raspap_realtime.sh`) RSSI values, output to `raspap_realtime.txt`
    2. then for **`Distance & velocity Calculation`** (`mainscript_1.py`), result output to a files `normal_distance.txt`,`normal_velocity.txt`, `time_distance.txt`, `obj_timedif_disance_velocity.txt` files.
    3. then for **`Kalman Filteration`** (`kalman_filter_design.py`) out put to files, `kalman_distance.txt`, `kalman_velocity.txt`
    4. Atlast `normal_velocity_update.py` based on kalman distance output, to a file `normal_velocity_update.py` and `time_interval.sh` for printing  the timestamp of sample from start  `0` to last_time.
    

**Note** : When you run `defaults.sh` and `defaults.sh --all`, before making them `default` all the files will be logged/appended to `Log_Files` folder. 