from ODSReader import ODSReader
import matplotlib.pyplot as plt
import numpy as np
ActualDistance = []
EvaluatedDistance_2 =[]
EvaluatedDistance_2_2 =[]
EvaluatedDistance_2_5 =[]
EvaluatedDistance_2_7 =[]
#ProbArray = []

####################################################For 1###############################
doc = ODSReader(u'Rasp_RSSI_distance_1_to_5_2.ods', clonespannedcolumns=True)
table = doc.getSheet(u'Sheet1')
N = len(table)
for i in range(len(table)):
   ActualDistance= np.append(ActualDistance, table[i][0]) #accessing elements in the 0 column
   EvaluatedDistance_2= np.append(EvaluatedDistance_2, table[i][1])
   EvaluatedDistance_2_2= np.append(EvaluatedDistance_2_2, table[i][2])
   EvaluatedDistance_2_5= np.append(EvaluatedDistance_2_5, table[i][3])
   EvaluatedDistance_2_7= np.append(EvaluatedDistance_2_7, table[i][4])




#print(PowerAnalysis_Distance)
#print(PowerAnalysis_RSSIArray)
#print(ProbArray)


ind = np.arange(N)
#PowerAnalysis_Distance=PowerAnalysis_Distance.astype(np.float) #converting string array to float
ActualDistance=ActualDistance.astype(np.float)
EvaluatedDistance_2=EvaluatedDistance_2.astype(np.float)
EvaluatedDistance_2_2=EvaluatedDistance_2_2.astype(np.float)
EvaluatedDistance_2_5=EvaluatedDistance_2_5.astype(np.float)
EvaluatedDistance_2_7=EvaluatedDistance_2_7.astype(np.float)

#ProbArray=ProbArray.astype(np.float) #converting string array to float



########################### Plotting ##################################

#p1=plt.plot(ind, EvaluatedDistance_2,label='n=2', marker='o' )
#p12=plt.plot(ind, EvaluatedDistance_2_2,label='n=2.2', marker='o' )
#p13=plt.plot(ind, EvaluatedDistance_2_5,label='n=2.5', marker='o' )
#p14=plt.plot(ind, EvaluatedDistance_2_7,label='n=2.7', marker='o' )
p1=plt.plot(ind, EvaluatedDistance_2,linestyle="-",label='n=2', marker='o' )
p12=plt.plot(ind, EvaluatedDistance_2_2,linestyle="--",label='n=2.2', marker='^' )
p13=plt.plot(ind, EvaluatedDistance_2_5,linestyle=":",label='n=2.5', marker='D' )
p14=plt.plot(ind, EvaluatedDistance_2_7,linestyle="-.",label='n=2.7', marker='X' )
plt.ylabel('Estimated object distance (m)')
plt.xlabel('Actual object distance (m)')
plt.xticks(ind, ActualDistance)
plt.yticks(np.arange(0, 6, 1))
plt.legend()
plt.grid()
plt.show()



