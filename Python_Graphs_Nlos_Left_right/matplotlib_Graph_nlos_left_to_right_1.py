from ODSReader import ODSReader
import matplotlib.pyplot as plt
import numpy as np
ActualDistance = []
EvaluatedDistance_1_8 =[]
EvaluatedDistance_2 =[]
EvaluatedDistance_2_5 =[]
#ProbArray = []

####################################################For 1###############################
doc = ODSReader(u'Rasp_RSSI_distance_left_to_right_1.ods', clonespannedcolumns=True)
table = doc.getSheet(u'Sheet1')
N = len(table)
for i in range(len(table)):
   ActualDistance= np.append(ActualDistance, table[i][0]) #accessing elements in the 0 column
   EvaluatedDistance_1_8= np.append(EvaluatedDistance_1_8, table[i][1])
   EvaluatedDistance_2= np.append(EvaluatedDistance_2, table[i][2])
   EvaluatedDistance_2_5= np.append(EvaluatedDistance_2_5, table[i][3])
   #EvaluatedDistance_2_7= np.append(EvaluatedDistance_2_7, table[i][4])




#print(PowerAnalysis_Distance)
#print(PowerAnalysis_RSSIArray)
#print(ProbArray)


ind = np.arange(N)
#PowerAnalysis_Distance=PowerAnalysis_Distance.astype(np.float) #converting string array to float
ActualDistance=ActualDistance.astype(np.float)
EvaluatedDistance_1_8=EvaluatedDistance_1_8.astype(np.float)
EvaluatedDistance_2=EvaluatedDistance_2.astype(np.float)
EvaluatedDistance_2_5=EvaluatedDistance_2_5.astype(np.float)
#EvaluatedDistance_2_7=EvaluatedDistance_2_7.astype(np.float)

#ProbArray=ProbArray.astype(np.float) #converting string array to float



########################### Plotting ##################################

p1=plt.plot(ind, EvaluatedDistance_1_8,linestyle="-.",label='n=1.8', marker='X' )
p12=plt.plot(ind, EvaluatedDistance_2, linestyle="-",label='n=2', marker='o' )
p13=plt.plot(ind, EvaluatedDistance_2_5,linestyle=":",label='n=2.5', marker='D' )
#p14=plt.plot(ind, EvaluatedDistance_2_7,label='n=2.7', marker='o' )
plt.ylabel('Estimated object distance (m)')
plt.xlabel('Actual object distance (m)')
plt.xticks(ind, ActualDistance)
plt.yticks(np.arange(-3, 3, 1))
plt.legend()
plt.grid()
plt.show()



